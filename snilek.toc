\babel@toc {czech}{}
\contentsline {section}{\numberline {1}Úvod}{3}%
\contentsline {section}{\numberline {2}Architektura}{4}%
\contentsline {subsection}{\numberline {2.1}Popis okolí microservice}{4}%
\contentsline {subsection}{\numberline {2.2}Popis architektury}{4}%
\contentsline {subsection}{\numberline {2.3}Logická struktura systému – rozdělení komponent}{5}%
\contentsline {subsection}{\numberline {2.4}Popis systémových vrstev}{5}%
\contentsline {subsection}{\numberline {2.5}Integrace s okolními systémy – návrh řešení integrace na technické úrovni}{5}%
\contentsline {subsection}{\numberline {2.6}Technologie – důvody použití}{5}%
\contentsline {subsection}{\numberline {2.7}HW infrastruktura a SW licence – sizing HW, licence}{6}%
\contentsline {subsection}{\numberline {2.8}Logické schéma HW infrastruktury (škálovatelnost...)}{6}%
\contentsline {section}{\numberline {3}Přihlášení}{7}%
\contentsline {section}{\numberline {4}Registrace}{10}%
\contentsline {section}{\numberline {5}Profil uživatele}{12}%
\contentsline {section}{\numberline {6}Chatbot}{14}%
\contentsline {section}{\numberline {7}Přehled úspor}{16}%
\contentsline {subsection}{\numberline {7.1}Seznam přání}{16}%
\contentsline {subsection}{\numberline {7.2}Grafy}{17}%
\contentsline {subsection}{\numberline {7.3}Příklad}{19}%
\contentsline {section}{\numberline {8}Odstranění účtu}{23}%
\contentsline {section}{\numberline {9}Validace}{24}%
\contentsline {subsection}{\numberline {9.1}User}{24}%
\contentsline {subsection}{\numberline {9.2}Chatbot}{25}%
\contentsline {section}{\numberline {10}Admin}{26}%
\contentsline {subsection}{\numberline {10.1}Vytvoření transakce}{26}%
\contentsline {section}{\numberline {11}Zabezpečení}{28}%
\contentsline {section}{\numberline {12}Usecase model}{29}%
\contentsline {section}{\numberline {13}Doménový model}{31}%
\contentsline {section}{\numberline {14}Požadavky}{32}%
\contentsline {subsection}{\numberline {14.1}Přehled funkčních požadavků}{32}%
\contentsline {subsection}{\numberline {14.2}Přehled nefunkčních požadavků}{32}%
\contentsline {section}{\numberline {15}Cenový odhad}{33}%
\contentsline {section}{\numberline {16}Projektový plán}{34}%
